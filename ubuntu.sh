sudo apt update && sudo apt install -y gnupg software-properties-common curl python3 pip git mc wget net-tools 

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"

curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py
python3 -m pip install --user ansible

curl -fsSL https://apt.releases.hashicorp.com/gpg | sudo apt-key add -
sudo apt-add-repository "deb [arch=amd64] https://apt.releases.hashicorp.com $(lsb_release -cs) main"
sudo apt update && sudo apt install -y terraform vagrant
terraform -help
touch ~/.bashrc
terraform -install-autocomplete

curl "https://awscli.amazonaws.com/awscli-exe-linux-x86_64.zip" -o "awscliv2.zip"
unzip awscliv2.zip
sudo ./aws/install
aws --version

# Graphical tools
sudo apt update && sudo apt install -y  plank gnome-system-monitor libreoffice vlc xubuntu-restricted-extras libavcodec-extra uget
sudo snap install intellij-idea-community --classic --edge
